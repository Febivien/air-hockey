﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public enum Winner
    {
        Player,
        Enemy
    }

    public static Winner winner;
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    } //readonly
    public delegate void RoundEnd();
    public static event RoundEnd OnRoundEnd = () => { };

    void OnEnable()
    {
        OnRoundEnd += TestWinConditions;
    }
    void OnDisable()
    {
        OnRoundEnd -= TestWinConditions;
    }

    public static void EndRound()
    {
        OnRoundEnd();
    }
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }
    private static void TestWinConditions()
    {
        if (winner == Winner.Player)
        {
            Debug.Log("Player wins");
        }
        else if (winner == Winner.Enemy)
        {
            Debug.Log("Enemy Wins");
        }
    }

}
