﻿using UnityEngine;

public class Helper : MonoBehaviour
{

    public GameObject[] playerArray = new GameObject[2];
    public GameObject[] markArray = new GameObject[2];
    public GameObject[] pucksArray = new GameObject[2];

    public  GameObject player1;
    public  GameObject player2;

    public  GameObject mark1;
    public  GameObject mark2;

    public  GameObject pucks1;
    public  GameObject pucks2;

    public  Rotate player1net;
    public  Rotate player2net;

    public  MarkBehaviour mark1net;
    public  MarkBehaviour mark2net;

    public static Transform target1;
    public static Transform target2;

	// Update is called once per frame
	void Update ()
	{
	    if (GameObject.FindGameObjectsWithTag("Player") != null)
	    {
	        playerArray = GameObject.FindGameObjectsWithTag("Player");
	    }

        if (GameObject.FindGameObjectsWithTag("Mark") != null)
        {
            markArray = GameObject.FindGameObjectsWithTag("Mark");
        }

        if (GameObject.FindGameObjectsWithTag("Puck") != null)
        {
            pucksArray = GameObject.FindGameObjectsWithTag("Pucks");
        }

        if (player1 == null && playerArray.Length >= 1)
	    {
	        player1 = playerArray[0];
	    }
	    if (player2 == null && playerArray.Length > 1)
	    {
	        player2 = playerArray[1];
	    }

        if (mark1 == null && markArray.Length >= 1)
        {
            mark1 = markArray[0];
        } 
        if (mark2 == null && markArray.Length > 1)
        {
            mark2 = markArray[1];
        }

        if (pucks1 == null && pucksArray.Length >= 1)
        {
            pucks1 = pucksArray[0];
        }
        if (pucks2 == null && pucksArray.Length > 1)
        {
            pucks2 = pucksArray[1];
        }

	    if (player1net == null)
	    {
            player1net = player1.GetComponent<Rotate>();
        }

        if (player2net == null && player2 != null)
        {
            player2net = player2.GetComponent<Rotate>();
        }

	    if (mark1net == null)
	    {
	        mark1net = mark1.GetComponent<MarkBehaviour>();
	    }

        if (mark2net == null && mark2 != null)
        {
            mark2net = mark2.GetComponent<MarkBehaviour>();
        }

	    if (target1 != null && mark1net != null)
	    {
	        mark1net.target = target1;
	    }
	    if (target2 != null && mark2net != null)
	    {
	        mark2net.target = target2;
	    }
    }

    public  void SetupPlayers()
    {
        player1net.playerMark = mark1;
        player2net.playerMark = mark2;
        player1net.playerPucks = pucks1;
        player2net.playerPucks = pucks2;
    }
}
