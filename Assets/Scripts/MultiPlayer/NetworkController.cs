﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkController : NetworkBehaviour {


    public GameObject pointer;
    public Rigidbody puck;
    public int speed;
    public bool fired;
    public Text isPlayer;
    public Text isSerwer;
	
	void Update ()
	{
	    isPlayer.text = isLocalPlayer.ToString();
	    isSerwer.text = isServer.ToString();    
        if (!isLocalPlayer)
        {
            return;
        }

	    if (Input.GetButtonDown("Player2Shoot"))
	    {
            fired = true;
            CmdShoot(puck.gameObject, pointer);
        }
	}

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    [Command]
    public void CmdShoot(GameObject puckToShoot, GameObject pointerToAim)
    {
        puckToShoot.GetComponent<Rigidbody>().velocity = transform.TransformDirection(pointer.transform.forward * speed);
        NetworkServer.Spawn(puckToShoot.gameObject);
    }
}
