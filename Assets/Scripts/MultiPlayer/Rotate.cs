﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Rotate : NetworkBehaviour
{

    public MarkBehaviour markBehaviour;
    public GameObject playerMarkTemp;
    public GameObject playerPucksTemp;
    public Text islocal;
    public List<GameObject> playerPuckList = new List<GameObject>(4);
    public int activePlayerPuck;
    public float speed;
    public float shootPower;
    public Rigidbody puck;
    private Transform thisTransform;
    public float smoothTime = 600f;
    public static bool isPlayer1 = true;

    public GameObject playerMarkPrefab;
    public GameObject playerPucksPrefab;

    public GameObject playerPucks;
    public GameObject playerMark;

    public Helper helper;

    void Awake()
    {
        activePlayerPuck = Random.Range(0, 4);
        helper = FindObjectOfType<Helper>();
    }

    public override void OnStartLocalPlayer()
    {
        CmdSpawn();

        foreach (Transform child in playerPucks.transform)
        {
            playerPuckList.Add(child.gameObject);
        }
    }

    [Command]
    void CmdSpawn()
    {
        playerMark =Instantiate(playerMarkPrefab,new Vector3(transform.position.x, transform.position.y + 12, transform.position.z), transform.rotation);
        playerPucks =Instantiate(playerPucksPrefab,new Vector3(transform.position.x, transform.position.y + 12, transform.position.z), transform.rotation);
        NetworkServer.SpawnWithClientAuthority(playerPucks,gameObject);
        NetworkServer.SpawnWithClientAuthority(playerMark, gameObject);
        StartCoroutine(wait());
    }

    void Update()
    {
        if (playerPucks == null)
        {
            helper.SetupPlayers();
            if(playerPuckList.Count == 0)
                if (playerPucks != null)
                    foreach (Transform child in playerPucks.transform)
                    {
                        playerPuckList.Add(child.gameObject);
                    }
        }

        if (puck == null)
        {
            puck = playerPuckList[activePlayerPuck].GetComponent<Rigidbody>();

        }

        if (isLocalPlayer)
        {
            if (Input.GetButtonDown("Player2Shoot"))
            {
                CmdShoot(gameObject);
            }
            CmdUpdatePosition();
        }

        if (playerPucks != null)
        {
            CmdAssignTarget();

        }
    }

    public void CmdShoot(GameObject pointerToAim)
    {
        puck.velocity = transform.TransformDirection(pointerToAim.transform.forward*shootPower);
    }

    public void CmdAssignTarget()
    {
        puck = playerPuckList[activePlayerPuck].GetComponent<Rigidbody>();
        Helper.target1 = puck.transform;
        Helper.target2 = puck.transform;
    }

    public void CmdUpdatePosition()
    {
        thisTransform = transform;
        thisTransform.position =
            new Vector3(Mathf.Lerp(thisTransform.position.x, puck.position.x, Time.deltaTime*smoothTime),
                Mathf.Lerp(thisTransform.position.y, puck.position.y, Time.deltaTime*smoothTime),
                Mathf.Lerp(thisTransform.position.z, puck.position.z, Time.deltaTime*smoothTime));
        transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0)*Time.deltaTime*speed);
    }

    IEnumerator wait()
    {
        yield return  new WaitForSeconds(5);
        helper.SetupPlayers();
    }
}
