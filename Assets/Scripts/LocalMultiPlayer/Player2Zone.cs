﻿using UnityEngine;
using System.Collections;

public class Player2Zone : MonoBehaviour {

    public MarkBehaviour markBehaviour;
    public GameObject enemyPucks;
    private ChangePuck changePuck;
    private ShootPuck shootPuck;
    private EnemyAI enemyAI;

    void Start()
    {
        shootPuck = FindObjectOfType<ShootPuck>();
        changePuck = FindObjectOfType<ChangePuck>();
        markBehaviour = FindObjectOfType<MarkBehaviour>();
        enemyAI = FindObjectOfType<EnemyAI>();
    }
    void OnTriggerEnter(Collider other)
    {
        other.gameObject.transform.parent.SetParent(enemyPucks.transform);
        changePuck.PlayerPuckList.Remove(other.gameObject.transform.parent.gameObject);
        enemyAI.enemyPuckList.Add(other.gameObject.transform.parent.gameObject);
        changePuck.ActivePlayerPuck = Random.Range(0, changePuck.PlayerPuckList.Count);

        markBehaviour.target = changePuck.PlayerPuckList[changePuck.ActivePlayerPuck].transform;
        shootPuck.Puck = changePuck.PlayerPuckList[changePuck.ActivePlayerPuck].GetComponent<Rigidbody>();
        shootPuck.Pointer.transform.position = shootPuck.Puck.gameObject.transform.position;
    }
}
