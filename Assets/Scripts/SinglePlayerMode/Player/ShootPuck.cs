﻿using UnityEngine;
public class ShootPuck : ShootPuckCore
{
    void Start()
    {
        Pointer = GameObject.FindGameObjectWithTag(PointerTag); // pointer tag "PlayerPointer";
    }

    void Update()
    {
        if (Input.GetButtonDown(InputName)) //fire2 and  Player2Shoot
        {
            Fired = true;
            Shoot();
        }
    }
}