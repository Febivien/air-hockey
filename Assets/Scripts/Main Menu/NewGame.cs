﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewGame : MonoBehaviour
{
    public static GameMode mode;

    public enum GameMode
    {
        Singleplayer,
        LocalMultiplayer,
        Multiplayer
    }

    [SerializeField] private Button playLocalMultiplayer;
    [SerializeField] private Button playVersusAI;
    [SerializeField] private EasyTween[] easyTween;
    [SerializeField] private Button startButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private AudioSource clickSound = null;

    public Button PlayLocalMultiplayer
    {
        get { return playLocalMultiplayer; }

        set { playLocalMultiplayer = value; }
    }
    public Button PlayVersusAI
    {
        get { return playVersusAI; }

        set { playVersusAI = value; }
    }
    public EasyTween[] EasyTween
    {
        get { return easyTween; }

        set { easyTween = value; }
    }
    public Button StartButton
    {
        get { return startButton; }

        set { startButton = value; }
    }
    public Button QuitButton
    {
        get { return quitButton; }

        set { quitButton = value; }
    }

    void Start()
    {
        PlayLocalMultiplayer.gameObject.SetActive(false);
        PlayVersusAI.gameObject.SetActive(false);
        StartButton.gameObject.SetActive(true);
        QuitButton.gameObject.SetActive(true);
    }

    public void StartLocalMultiplayer()
    {
        mode = GameMode.LocalMultiplayer;
        StartCoroutine(wait("LocalMultiplayer"));
    }
    public void StartSinglePlayer()
    {
        mode = GameMode.Singleplayer;
        StartCoroutine(wait("SinglePlayer"));
    }
    public void StartGame()
    {
        StartCoroutine(waiter());
    }

    private IEnumerator waiter()
    {
        clickSound.Play();

        yield return new WaitForSeconds(.29f);

        playVersusAI.gameObject.SetActive(true);
        playLocalMultiplayer.gameObject.SetActive(true);
        StartButton.gameObject.SetActive(false);
        QuitButton.gameObject.SetActive(false);
        for (int i = 0; i < EasyTween.Length; i++)
        {
            EasyTween[i].OpenCloseObjectAnimation();
        }
    }

    private IEnumerator wait(string scene)
    {

        clickSound.Play();

        yield return new WaitForSeconds(.29f);

        SceneManager.LoadScene(scene);

    }
}
