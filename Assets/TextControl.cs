﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TextControl : MonoBehaviour
{

    #region Variables
    [SerializeField]
    private RotatePointerWithMouse rotatePointerWithMouse;
    [SerializeField]
    private EnemyAI enemyAi;
    [SerializeField]
    private AudioSource clip;
    [SerializeField]
    private AudioSource clip1;
    [SerializeField]
    private Image display;
    [SerializeField]
    private RectTransform displayRect;

    private readonly Vector2 displaySize = new Vector2(300, 100);
    private int index;
    private bool isPlayingClip;
    private int seconds;
    [SerializeField]
    private Sprite[] spritesOfNumbers;
    private float timeLeft = 3f;
    private readonly WaitForSeconds wait = new WaitForSeconds(1);
    #endregion

    #region Properites
    public AudioSource Clip
    {
        get { return clip; }
    }
    public AudioSource Clip1
    {
        get { return clip1; }
    }
    public Image Display
    {
        get { return display; }

    }
    public RectTransform DisplayRect
    {
        get { return displayRect; }
    }
    public bool IsPlaying { get; set; }
    public Sprite[] SpritesOfNumbers
    {
        get { return spritesOfNumbers; }
    }
    public float TimeLeft
    {
        get { return timeLeft; }

        set { timeLeft = value; }
    }
    public RotatePointerWithMouse RotatePointerWithMouse
    {
        get { return rotatePointerWithMouse; }

        set { rotatePointerWithMouse = value; }
    }
    public EnemyAI EnemyAi
    {
        get { return enemyAi; }

        set { enemyAi = value; }
    }
    #endregion

    #region Functions

    private void Start()
    {
        EnemyAi = FindObjectOfType<EnemyAI>();
        RotatePointerWithMouse = FindObjectOfType<RotatePointerWithMouse>();
        EnemyAi.enabled = false;
        RotatePointerWithMouse.enabled = false;
        isPlayingClip = true;
        TimeLeft += 2;
        IsPlaying = true;
        Display.sprite = SpritesOfNumbers[0];
        DisplayRect.sizeDelta = new Vector2(100, 100);
        StartCoroutine(Play);
    }

    private void Update()
    {
        TimeLeft -= Time.deltaTime;
        TimeLeft = Mathf.Clamp(TimeLeft, 0, 5);
        seconds = (int)TimeLeft;
        Display.sprite = SpritesOfNumbers[seconds];
        if (Display.sprite == SpritesOfNumbers[0])
        {
            DisplayRect.sizeDelta = displaySize;
            StartCoroutine(WaitSecondAndDestroy());
        }
    }

    #endregion

    #region IEnumerators

    private IEnumerator WaitSecondAndDestroy() { 
    
            yield return wait;
            RotatePointerWithMouse.enabled = true;
            EnemyAi.enabled = true;
            gameObject.SetActive(false);
    }

    private IEnumerator Play
    {
        get
        {
            while (IsPlaying)
            {
                if (isPlayingClip)
                    Clip.Play();
                else
                    Clip1.Play();
                index++;
                if (index == 4)
                    isPlayingClip = false;
                yield return wait;
            }
        }
    }
    #endregion
}